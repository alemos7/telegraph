package com.formacionbdi.springboot.app.telegraph.service.morse;


public class MorseRequest {
	
	private String velocity_operator = null;
	private MorseRule rule;
	private MorseTranslate translate;

	public MorseRequest() {
	}
	
	public MorseRequest(String velocity_operator, MorseRule rule, MorseTranslate translate) {
		this.velocity_operator = velocity_operator;
		this.rule = rule;
		this.translate = translate;
	}
	
	public String getVelocity_operator() {
		return velocity_operator;
	}
	public void setVelocity_operator(String velocity_operator) {
		this.velocity_operator = velocity_operator;
	}
	public MorseRule getRule() {
		return rule;
	}
	public void setRule(MorseRule rule) {
		this.rule = rule;
	}
	public MorseTranslate getTranslate() {
		return translate;
	}
	public void setTranslate(MorseTranslate translate) {
		this.translate = translate;
	}
	
	
}
