package com.formacionbdi.springboot.app.telegraph.service;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.formacionbdi.springboot.app.telegraph.models.entity.Rule;

@Repository
public interface IRuleService extends MongoRepository<Rule, String> {
	public Rule findByMethod(String method);
	public Rule findByMethodAndVelocity(String method, String Velocity);
	public List<Rule> findByVelocity(String velocity);
}
