package com.formacionbdi.springboot.app.telegraph.service.human;

public class HumanRule {
	private String sparator = null;
	private String end_first = null;
	private String end_second = null;
	

	public HumanRule() {
	}
	
	public HumanRule(String sparator, String end_first, String end_second) {
		this.sparator = sparator;
		this.end_first = end_first;
		this.end_second = end_second;
	}
	
	public String getSparator() {
		return sparator;
	}
	public void setSparator(String sparator) {
		this.sparator = sparator;
	}
	public String getEnd_first() {
		return end_first;
	}
	public void setEnd_first(String end_first) {
		this.end_first = end_first;
	}
	public String getEnd_second() {
		return end_second;
	}
	public void setEnd_second(String end_second) {
		this.end_second = end_second;
	}

	
	
	
}
