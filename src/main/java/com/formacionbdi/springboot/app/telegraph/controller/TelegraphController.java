package com.formacionbdi.springboot.app.telegraph.controller;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.formacionbdi.springboot.app.telegraph.models.entity.Letter;
import com.formacionbdi.springboot.app.telegraph.models.entity.Rule;
import com.formacionbdi.springboot.app.telegraph.service.ILatterService;
import com.formacionbdi.springboot.app.telegraph.service.RuleServiceImpl;
import com.formacionbdi.springboot.app.telegraph.service.bits.BitsRequest;
import com.formacionbdi.springboot.app.telegraph.service.human.HumanRequest;
import com.formacionbdi.springboot.app.telegraph.service.morse.MorseRequest;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Metrics;

@RestController
public class TelegraphController {

	final 	String 	ERROR = "No fue posible realizar la traducción";
	final 	Integer ERROR_CODE = 500;
	final 	Integer SUCESS_CODE = 200;
	
	private String 	translation="";
	private String 	human="";
	private String 	bits="";
	private String 	morse="";
	
	@Autowired
	private ILatterService letterService;
	
	@Autowired
	private RuleServiceImpl ruleService;

	@PostMapping("bits2morse")
	public ResponseEntity<?> bits2morse(@RequestBody BitsRequest request) {
		
		BitsController morse = new BitsController();
		this.translation = new String (Thread.currentThread().getStackTrace()[1].getMethodName());
		this.bits = request.getTranslate().getText();
		this.morse = "";
		
		if(Objects.equals(request.getTranslate().getMorse(), new String("true"))) {
			this.morse = morse.decodeBits2Morse(request, ruleService, this.translation);
			if(this.morse == null) return response(this.ERROR_CODE);
			if(Objects.equals(request.getTranslate().getHuman(), new String("true"))) {
				MorseController human = new MorseController();
				this.human = human.translate2Human(null, letterService, ruleService, "morse2human", request.getVelocity_operator(), this.morse);
				if(this.human == null) return response(this.ERROR_CODE);
			}else{
				this.human = "";
			}
		}else {
			if(Objects.equals(request.getTranslate().getHuman(), new String("true"))) {
				this.morse = "";
				this.human = this.ERROR;
			}else {
				this.human = "";
			}
		}
		this.entryReport(this.translation, request.getVelocity_operator());
		return response(this.SUCESS_CODE);
	}

	@PostMapping("morse2human")
	public ResponseEntity<?> morse2human(@RequestBody MorseRequest request) {	
		
		MorseController human = new MorseController();
		this.translation = new String (Thread.currentThread().getStackTrace()[1].getMethodName());
		this.morse = request.getTranslate().getText();
		
		if(Objects.equals(request.getTranslate().getHuman(), new String("true"))) {
			this.human = human.translate2Human(request, letterService, ruleService, this.translation, request.getVelocity_operator(), null);
			if(this.human == null) return response(this.ERROR_CODE);
		}else {
			this.human = "";
		}
		if(Objects.equals(request.getTranslate().getBits(), new String("true"))) {
			Morse2Bits bits = new Morse2Bits();
			this.bits = bits.translateMorse2Bits(this.morse, ruleService, request.getVelocity_operator());
			if(this.bits == null) return response(this.ERROR_CODE);
		}else {
			this.bits = "";
		}
		this.entryReport(this.translation, request.getVelocity_operator());
		return response(this.SUCESS_CODE);
	}
	
	@PostMapping("human2morse")
	public ResponseEntity<?> human2morse(@RequestBody HumanRequest request) {	
		
		HumanController morse = new HumanController();
		this.translation = new String (Thread.currentThread().getStackTrace()[1].getMethodName());
		this.human = request.getTranslate().getText();
		if(this.human == null) return response(this.ERROR_CODE);
		if(Objects.equals(request.getTranslate().getMorse(), new String("true"))) {
			this.morse = morse.human2morse(request, ruleService, letterService, this.translation, request.getTranslate().getText());
			if(this.morse == null) return response(this.ERROR_CODE);
			
			if(Objects.equals(request.getTranslate().getBits(), new String("true"))) {
				Morse2Bits bits = new Morse2Bits();
				this.bits = bits.translateMorse2Bits(this.morse, ruleService, request.getVelocity_operator());
				if(this.bits == null) return response(this.ERROR_CODE);
			}else {
				this.bits = "";
			}
		}else {
			if(Objects.equals(request.getTranslate().getBits(), new String("true"))) {
				this.morse = "";
				this.bits = this.ERROR;
			}else {
				this.morse = "";
				this.bits = "";
			}
		}
		this.entryReport(this.translation, request.getVelocity_operator());
		return response(this.SUCESS_CODE);
	}
	
	@GetMapping("letter/list")
	public List<Letter> listar(){
		return letterService.findAll();
	}
	
	@GetMapping("letter/id/{id}")
	public Letter detalle(@PathVariable Long id) {
		return letterService.findById(id);
	}
	
	@GetMapping("letter/{letter}")
	public Letter letra(@PathVariable String letter) {
		return letterService.findByLetter(letter);
	}
	
	public ResponseEntity<?> response(Integer code) {
		ResponseController result = new ResponseController();
		return result.response(
			code,
			this.translation,
			this.human,
			this.bits,
			this.morse
		);
	}
	
	@GetMapping("rule/get")
	public Rule getRule(@RequestParam String method) {
		return ruleService.getByMethod(method);
	}
	
	@GetMapping("rule/getMethodVelocity")
	public Rule getRuleMethodVelocity(@RequestParam String method, String velocity) {
		return ruleService.getByMethodVelocity(method, velocity);
	}
	
	@GetMapping("/rule")
	public List<Rule> getAll() {
		return ruleService.getAll();
	}
	
	public String getTranslation() {
		return translation;
	}

	public void setTranslation(String translation) {
		this.translation = translation;
	}

	public String getHuman() {
		return human;
	}

	public void setHuman(String human) {
		this.human = human;
	}

	public String getBits() {
		return bits;
	}

	public void setBits(String bits) {
		this.bits = bits;
	}

	public String getMorse() {
		return morse;
	}

	public void setMorse(String morse) {
		this.morse = morse;
	}
	
	public void entryReport(String method, String velocity) {
	    Counter counter =Metrics.counter("request.translation",  "method",    
	    		method, "velocity", velocity);   
	    counter.increment();
	}
	
}
