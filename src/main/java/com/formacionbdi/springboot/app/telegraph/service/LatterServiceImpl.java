package com.formacionbdi.springboot.app.telegraph.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacionbdi.springboot.app.telegraph.models.dao.LetterDao;
import com.formacionbdi.springboot.app.telegraph.models.entity.Letter;

@Service
public class LatterServiceImpl implements ILatterService{

	@Autowired
	private LetterDao latterDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Letter> findAll() {
		return (List<Letter>) latterDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Letter findById(Long id) {
		return latterDao.findById(id).orElse(null);
	}

	@Override
	public Letter findByLetter(String letter) {
		return latterDao.findByLetter(letter);
	}

	@Override
	public Letter findByMorse(String morse) {
		return latterDao.findByMorse(morse);
	}

}
