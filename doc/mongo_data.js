db.getCollection('rule').find({})


{
    "_id" : ObjectId("5eac6642d08c93eafebaad79"),
    "method" : "bits2morse",
    "velocity" : "fast",
    "rule" : {
        "init" : "00000000",
        "point" : "11",
        "stripe" : "1111",
        "space" : "00",
        "sparator" : "0000",
        "between" : "000",
        "end_first" : "00000000",
        "end_second" : "1100111100110011110011001111"
    }
}

{
    "_id" : ObjectId("5eac8eb0d08c93eafebabc95"),
    "method" : "morse2human",
    "velocity" : "fast",
    "rule" : {
        "short_value" : ".",
        "long_value" : "-",
        "space" : " ",
        "sparator" : "   ",
        "end_first" : ".-.-.-",
        "end_second" : "        "
    }
}

{
    "_id" : ObjectId("5eacb564d08c93eafebacc06"),
    "method" : "human2morse",
    "velocity" : "fast",
    "rule" : {
        "sparator" : " ",
        "end_first" : ".",
        "end_second" : "."
    }
}