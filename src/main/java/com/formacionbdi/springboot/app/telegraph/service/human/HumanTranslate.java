package com.formacionbdi.springboot.app.telegraph.service.human;

public class HumanTranslate {
	private String text = null;
	private String bits = null;
	private String morse = null;
	

	public HumanTranslate() {
	}
	
	public HumanTranslate(String text, String bits, String morse) {
		this.text = text;
		this.bits = bits;
		this.morse = morse;
	}
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getBits() {
		return bits;
	}
	public void setBits(String bits) {
		this.bits = bits;
	}
	public String getMorse() {
		return morse;
	}
	public void setMorse(String morse) {
		this.morse = morse;
	}

	
	
}
