package com.formacionbdi.springboot.app.telegraph.service.bits;

public class BitsTranslate {
	private String text = null;
	private String morse = null;
	private String human = null;
	
	
	public BitsTranslate() {
	}
	
	public BitsTranslate(String text, String morse, String human) {
		super();
		this.text = text;
		this.morse = morse;
		this.human = human;
	}
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getMorse() {
		return morse;
	}
	public void setMorse(String morse) {
		this.morse = morse;
	}
	public String getHuman() {
		return human;
	}
	public void setHuman(String human) {
		this.human = human;
	}
}
