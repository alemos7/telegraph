package com.formacionbdi.springboot.app.telegraph.service;

import java.util.List;

import com.formacionbdi.springboot.app.telegraph.models.entity.Letter;

public interface ILatterService {
	
	public List<Letter> findAll();
	public Letter findById(Long id);

	public Letter findByLetter(String c);

	public Letter findByMorse(String morse);
	
	//@Query("SELECT letter, morse FROM mapper WHERE letter in('P', 'A', 'P', 'A')")
	//public List<Letter> findByMorse(String morse);
}
