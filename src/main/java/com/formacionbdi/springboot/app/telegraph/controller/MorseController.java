package com.formacionbdi.springboot.app.telegraph.controller;

import org.json.JSONObject;

import com.formacionbdi.springboot.app.telegraph.service.ILatterService;
import com.formacionbdi.springboot.app.telegraph.service.RuleServiceImpl;
import com.formacionbdi.springboot.app.telegraph.service.morse.MorseRequest;

public class MorseController {
	
	private String text;
	private String short_value;
	private String long_value;
	private String space;
	private String sparator;
	private String end_first;
	private String end_second;
	private String translation = "";
	
	public String translate2Human(MorseRequest request, ILatterService letterService, RuleServiceImpl ruleService, String method, String velocity, String text) {
		this.getRule(ruleService, request, method, velocity, text);
		String[] exploded = this.text.split(this.sparator);
		for(int i=0;i<exploded.length ; i++)
		{
			String[] explodedLetter = exploded[i].split(this.space);
			for(int ii=0;ii<explodedLetter.length ; ii++)
			{
				this.translation += letterService.findByMorse(explodedLetter[ii]).getLetter();
			}
			if(exploded.length > 1 && exploded.length-1 != i) this.translation += " ";
		}
				
		return this.translation;
	}

	public String getTranslation() {
		return translation;
	}

	public void setTranslation(String translation) {
		this.translation = translation;
	}
	
	public void getRule(RuleServiceImpl ruleService, MorseRequest request, String method, String velocity, String text) {
		
		if(validator(request)) {
			this.text			= request.getTranslate().getText().toUpperCase();
			this.short_value	= request.getRule().getShort_value();
			this.long_value		= request.getRule().getLong_value();
			this.space 			= request.getRule().getSpace();
			this.sparator 		= request.getRule().getSparator();
			this.end_first 		= request.getRule().getEnd_first();
			this.end_second 	= request.getRule().getEnd_second();
		}else {
			if(text == null) text= request.getTranslate().getText();
			
			JSONObject rule = new JSONObject(ruleService.getByMethodVelocity(method, velocity).getRule());
			this.short_value	= rule.getString("short_value");
			this.long_value		= rule.getString("long_value");
			this.space 			= rule.getString("space");
			this.sparator		= rule.getString("sparator");
			this.end_first		= rule.getString("end_first");
			this.end_second 	= rule.getString("end_second");
			this.text			= text.toUpperCase();
			
		}
		
		
	}
	
	@SuppressWarnings("unlikely-arg-type")
	public Boolean validator(MorseRequest request) {
		
		if(request == null) {
			return false;
		}
		if(request.getRule() == null) {
			return false;
		}		
		
		if (
			request.getRule().getShort_value() != "" &&
			request.getRule().getLong_value() != "" &&
			request.getRule().getSpace() != "" &&
			request.getRule().getSparator() != ""
			) {
			return true;
		}
		return false;
	}
	
	
	
}
