package com.formacionbdi.springboot.app.telegraph.service.human;

public class HumanRequest {
	
	private String velocity_operator = null;
	private HumanRule rule;
	private HumanTranslate translate;
	
	public HumanRequest() {
	}
	
	public HumanRequest(String velocity_operator, HumanRule rule, HumanTranslate translate) {
		this.velocity_operator = velocity_operator;
		this.rule = rule;
		this.translate = translate;
	}
	
	public String getVelocity_operator() {
		return velocity_operator;
	}
	
	public void setVelocity_operator(String velocity_operator) {
		this.velocity_operator = velocity_operator;
	}
	
	public HumanRule getRule() {
		return rule;
	}
	
	public void setRule(HumanRule rule) {
		this.rule = rule;
	}
	
	public HumanTranslate getTranslate() {
		return translate;
	}
	
	public void setTranslate(HumanTranslate translate) {
		this.translate = translate;
	}
	
	
	
}
