package com.formacionbdi.springboot.app.telegraph.service.bits;

public class BitsRule {
	private String init = null;
	private String point = null;
	private String stripe = null;
	private String space = null;
	private String sparator = null;
	private String between = null;
	public String getBetween() {
		return between;
	}

	public void setBetween(String between) {
		this.between = between;
	}
	private String end_first = null;
	private String end_second = null;
	
	public BitsRule(String init, String point, String stripe, String space, String sparator, String between, String end_first,
			String end_second) {
		super();
		this.init = init;
		this.point = point;
		this.stripe = stripe;
		this.space = space;
		this.sparator = sparator;
		this.between = between;
		this.end_first = end_first;
		this.end_second = end_second;
	}
	
	public BitsRule() {
	}

	public String getInit() {
		return init;
	}
	public void setInit(String init) {
		this.init = init;
	}
	public String getPoint() {
		return point;
	}
	public void setPoint(String point) {
		this.point = point;
	}
	public String getStripe() {
		return stripe;
	}
	public void setStripe(String stripe) {
		this.stripe = stripe;
	}
	public String getSpace() {
		return space;
	}
	public void setSpace(String space) {
		this.space = space;
	}
	public String getSparator() {
		return sparator;
	}
	public void setSparator(String sparator) {
		this.sparator = sparator;
	}
	public String getEnd_first() {
		return end_first;
	}
	public void setEnd_first(String end_first) {
		this.end_first = end_first;
	}
	public String getEnd_second() {
		return end_second;
	}
	public void setEnd_second(String end_second) {
		this.end_second = end_second;
	}
	
	
}
