## Telegraph: Software para la traducción (Bits – Morse – Humano)

Luego, de ser propuesto el uso del telégrafo para comunicaciones entre distintos equipos utilizando código MORSE.  Se decide diseñar una solución tecnológica que permita identificar los mensajes de cada uno de los emisores dentro de la empresa. 

Un problema implícito para la interpretación de los mensajes en MORSE, es la velocidad de trasmisión de los mismos. Naturalmente, la velocidad de transmisión del código varía ligeramente a lo largo del mensaje ya que, es enviada por un operador humano. 

Partiendo de la premisa anterior, se desarrolla el siguiente ejercicio que no solo apunta a la propuesta directamente, sino, se analizan otras arista asociadas al funcionamiento correcto y la alta escalabilidad, persiguiendo en todo momento mitigar el posible error humano que puede existir en el medio de la transmisión. 

Lo antes expresado, se busca una solución 360°, que si bien es cierto, sólo se solicita la traducción de una cierta información, sin dejar de analizar temas técnicos, como por ejemplo: velocidad del operador, posibles reglas de transformación adaptadas a cada situación, clasificación de los operadores de acuerdo a su velocidad de transmisión, de éstas no existir el sistema asume en su momento las reglas predeterminadas para dicho escenario.

En el mismo orden de ideas, se hace énfasis en la velocidad, escalabilidad y fiabilidad de la información que se entrega al usuario final, esto como elementos básicos de la comunicación, poniendo la tecnología siempre a favor de la humanidad, cuidando esos posible errores humanos que pongan en riesgo el mensaje que se desea transmitir. 

# Demo : 

- Collection de Postman: https://www.getpostman.com/collections/f1efe234d8e1819cbba6
- Swagger: http://18.191.126.120:8777/swagger-ui.html#/telegraph-controller
- login: 
	- Usuario (Operador): alemos
	- Clave: 12345
	- Usuario (Admin): admin
	- Clave: 12345
- Eureka: http://18.191.126.120:8761/
- Zipkin: http://18.191.126.120:9411/zipkin/
- RabbitMQ: http://18.191.126.120:15672/#/
	- usuario: guest
	- clave: guest
- Cronograf: http://18.191.126.120:8888/sources/0/status
	- Al ingresar ir a: Dashboards -> Request Services
- Dcoker: ./docker-composer/
- Documentación general: ./doc (Los diferentes archivos muestran como pocos a poco se fue alcanzado la madurez de la app)


# Tecnologías Implementadas : 

-     Lenguaje de programación JAVA 
-     Framework usado Spring boot 
-     Estructura Microservicios
	-     Eureka (Orquestador)
	-     Zuul (Api gateway)
	-     Zinpik (Observador de Trazas)
	-     RabbitMQ (Mensajería)
	-      Oauth0 (Autenticación) 
-     Base de datos
	-     Mysql (Relacional)
	-     MongoDB (No relacional)
	-     InfluxDB (Time Serial) 
-      Git (Versionado del código Gitflow) 
-     Docker – Docker composer – Dockerhub (Contenedor del Código)
-     Cloud (Aws – EC2) 


## Dando un enfoque más técnico

La arquitectura de microservicios es una aproximación para el desarrollo de software que consiste en construir una aplicación como un conjunto de pequeños servicios, los cuales se ejecutan en su propio proceso y se comunican con mecanismos ligeros (normalmente una API de recursos HTTP). Cada servicio se encarga de implementar una funcionalidad completa del negocio. Cada servicio es desplegado de forma independiente y puede estar programado en distintos lenguajes y usar diferentes tecnologías de almacenamiento de datos. En pocas palabras, el objetivo es distribuir sistemas de software de calidad con mayor rapidez, lo cual es posible gracias a los microservicios, pero también se deben considerar otros aspectos. Dividir las aplicaciones en microservicios no es suficiente; es necesario administrarlos, coordinarlos y gestionar los datos que crean y modifican.

## Eureka
La arquitectura de microservicios es una aproximación para el desarrollo de software que consiste en construir una aplicación como un conjunto de pequeños servicios, los cuales se ejecutan en su propio proceso y se comunican con mecanismos ligeros (normalmente una API de recursos HTTP). Cada servicio se encarga de implementar una funcionalidad completa del negocio. Cada servicio es desplegado de forma independiente y puede estar programado en distintos lenguajes y usar diferentes tecnologías de almacenamiento de datos. En pocas palabras, el objetivo es distribuir sistemas de software de calidad con mayor rapidez, lo cual es posible gracias a los microservicios, pero también se deben considerar otros aspectos. Dividir las aplicaciones en microservicios no es suficiente; es necesario administrarlos, coordinarlos y gestionar los datos que crean y modifican.

### Zuul
se puede definir como un proxy inverso o edge service que nos va a permitir tanto enrutar y filtrar nuestras peticiones de manera dinámica, como monitorizar y securizar las mismas. Este componente actúa como un punto de entrada a nuestros servicios, es decir, se encarga de solicitar una instancia de un microservicio concreto a Eureka y de su enrutamiento hacia el servicio que queramos consumir.

### Zipkin
El auge de las arquitecturas de microservicios ha traído consigo algunos retos que debemos ser capaces de abordar para conseguir un sistema consistente, como son la monitorización, gestión de la configuración centralizada, centralización de logs… En este tipo de arquitecturas una petición de un consumidor de nuestro sistema puede desencadenar varias llamadas internas entre microservicios, por lo que es importante poder asociar un identificador único de petición para que se pueda propagar entre estas llamadas y así poder posteriormente consultar las peticiones de forma centralizada.

### RabbitMQ
RabbitMQ es un software de encolado de mensajes llamado bróker de mensajería o gestor de colas. Dicho de forma simple, es un software donde se pueden definir colas, las aplicaciones se pueden conectar a dichas colas y transferir/leer mensajes en ellas. Sus características principales son:

### Oauth0
Open Authorization (OAuth) es un estándar abierto que permite flujos simples de autorización para sitios web o aplicaciones informáticas. Se trata de un protocolo propuesto por Blaine Cook y Chris Messina, que permite autorización segura de una API de modo estándar y simple para aplicaciones de escritorio, móviles y web. 

## Solución propuesta:
De acuerdo a lo planteado y teniendo en contexto las tecnologías antes descritas, se lleva a cabo un desarrollo central complementado con los demás microservicios satélites que lo robustecen y hacen que la solución propuesta tenga un abarque 360°, poniéndonos en el centro como cliente y haciéndonos las siguientes pregunta:
 
“Si soy un cliente que necesito transmitir la información, que me gustaría que ocurriese, ¿Fiabilidad?, ¿Calidad de la información? ¿Rapidez de la transmisión?, ¿Siempre disponible?”. 

Por otra parte, pero para nada menos importante. “Si soy un operador dado que el enunciado hace referencia a que la aplicación será usada por los mismos, nos hacernos las siguientes preguntas ¿Como me sentiría mas cómodo con la herramienta? ¿Como me ayudaría a cometer menos errores? ¿Que opciones de respuesta tendría para ser mas productivo al cliente?, ¿Que tan rápido debo de ser para entrar en las referencias de traducción”.

“Si soy un técnico, ¿Que me gustaría ver en cada una de las transmisiones?, ¿Que debería de estar mirando? y ¿Con que posibles alertas contaría si algo ocurriese?. 

Partiendo de todas aquellas preguntas, posturas y situaciones presentes, se analiza la posibilidad de implementar un conjunto de reglas de traducción que serán usadas para descifrar el texto indicado, dichas reglas están planteadas en base a una velocidad del operador, no sería justo pensar que todos los operadores son rápidos, lentos o todos forman parte de un grupo de operadores de velocidad media, por tal razón, se plantea una funcionalidad de tipificar a un operador basado en su velocidad, aquel operador que no se encuentre identificado bajo una velocidad, el sistema por temas técnicos y en pro de asegurar la rapidez de la entrega de la información, estará levantando la configuración de reglas mas optimas. Es por todo esto indicado que el desarrollo se inicia teniendo 3 premisas particulares, todas estas bajo la siguiente definición, es necesario que sea indicado un tipo de traducción, velocidad, regla y resultado esperado, los tres tipos de traducción son: 

#### bits2morse:
Funcionalidad que espera recibir entre su conjunto de datos, una cadena de texto en bits que se desea transformada en código morse, abriendo así la posibilidad de traducción a un lenguaje entendible por humano.
```json
{
	"velocity_operator": "fast",
	"rule": {
	    "init": "00000000",
	    "point": "11",
	    "stripe": "1111",
	    "space": "00",
	    "sparator": "0000",
	    "between": "000",
	    "end_first": "00000000"
    },
    "translate": {
    	"text": "0000000011001100110011000111100111100111100011001111001100110001100111100001111001111000110001100111100110011000110011",
    	"morse": "true",
    	"human": "true"
    }
}
{
	"velocity_operator": "midium",
	"rule": {
	    "init" : "0000000000000000",
        "point" : "1111",
        "stripe" : "11111111",
        "space" : "0000",
        "sparator" : "0000000000",
        "end_1" : "0000000000000000",
        "end_2" : "11110000111111110000111100001111111100001111000011111111"
    },
    "translate": {
    	"text": "{{ Cadena de bits que corresponda }}",
    	"morse": "true",
    	"human": "true"
    }
}
{ 
	"velocity_operator": "slow",
	"rule": {
	    "init" : "000000000000000000000000",
        "point" : "111111",
        "stripe" : "111111111111",
        "space" : "000000",
        "sparator" : "000000000000000",
        "end_1" : "000000000000000000000000",
        "end_2" : "111111000000111111111111000000111111000000111111111111000000111111000000111111111111"
    },
    "translate": {
    	"text": "{{ Cadena de bits que corresponda }}",
    	"morse": "true",
    	"human": "true"
    }
}


```
#### Morse2human
Funcionalidad que espera recibir entre sus conjunto de datos una cadena de texto expresada en código morse, considerando una tabla de referencia particular propia de la solución y finalmente entregar un mensaje en lenguaje para humano, dando así la posibilidad de llevarlo a bits. 
```json
{
	"velocity_operator": "fast",
	"rule": {
	    "short_value": ".",
	    "long_value": "-",
	    "space": " ",
	    "sparator": "   ",
	    "end_first": ".-.-.-",
	    "end_second": "        "
    },
    "translate": {
    	"text": ".... --- .-.. .-   -- . .-.. ..",
    	"bits": "true",
    	"human": "true"
    }
}
```
#### Human2morse
Funcionalidad que espera recibir entre sus parámetros una cadena de texto y se espera su traducción en código morse, considerando como referencia para la traducción una tabla propia de la solución, dicha funcionalidad brinda la posibilidad de entregar como resultado una cadena de texto en bits.
```json
{
	"velocity_operator": "fast",
	"rule": {
	    "sparator": " ",
	    "end_first": ".",
	    "end_second": "."
    },
    "translate": {
    	"text": "HOLA TELEGRAPH",
    	"bits": "true",
    	"morse": "true"
    }
}
```
###Como funcionalidades complementarías se tienen
  - Listar todas las letras y su referencia en código morse. 
  - Buscar una letra o número particular y saber de esta manera su referencia de traducción en código morse
  - Listar las reglas de traducción por velocidad y tipo de traducción 

## Flujo de Funcionamiento usuario:
[![Flujo Actual](https://lh5.googleusercontent.com/FUdMMFTctdrx09ZkCuXO0UBvT55TwgM0GTY8UbZbdhJcT9aF9RLvVgycEPMaiORTmH8kFuSasuzvBA=w1920-h946 "Flujo Actual")](https://lh5.googleusercontent.com/FUdMMFTctdrx09ZkCuXO0UBvT55TwgM0GTY8UbZbdhJcT9aF9RLvVgycEPMaiORTmH8kFuSasuzvBA=w1920-h946 "Flujo Actual")

## Flujo de Procesamiento de Datos
[![Flujo de procesamiento de datos](https://lh5.googleusercontent.com/KQ33hPutX9XWpJ40PDMkgBLxdDV95p7bsrBZbg-aPhMbz_0QY3NB4K9eRZsOKCOyEOJzHahIEQZGtg=w1920-h946 "Flujo de procesamiento de datos")](https://lh5.googleusercontent.com/KQ33hPutX9XWpJ40PDMkgBLxdDV95p7bsrBZbg-aPhMbz_0QY3NB4K9eRZsOKCOyEOJzHahIEQZGtg=w1920-h946 "Flujo de procesamiento de datos")

## DFD
[![Diagrama de flujo de datos](https://drive.google.com/file/d/1SVYBIxDg78sbs5Y5IrTQAVzk3gI6FNTd/view?usp=sharing "Diagrama de flujo de datos")](https://drive.google.com/file/d/1SVYBIxDg78sbs5Y5IrTQAVzk3gI6FNTd/view?usp=sharing "Diagrama de flujo de datos")

## Puntos de Mejora:
Sin duda, que la solución posee un sin fin de puntos de mejoras, tanto a nivel funcional para los distintos usuarios, como en la comunicación de los microservicios internamente. Entre esos puntos de mejoras se plantea la siguiente arquitectura: 

Es posible observar varios componentes nuevos: Diccionario, Capa de cache (Redis), entre otros.

La idea sería poder contemplar una nueva capa de cache para brindar respuesta mas rápida, así como también, contar con un nuevo microservicio llamado diccionario que su finalidad será almacenar cada nueva palabra y su traducción en los tres diferentes tipos de traducción y velocidad para que de alguna manera optimicemos los tiempos de respuesta en vez de ir traduciendo cada una de las palabras indicadas, se consultaría el mismo y de existir se entregaría, es importante resaltar que estas palabras a registrar deben ser previamente validada tanto en el idioma como gramaticalmente, es por ello, que deberá ser posible integrarse de alguna manera con interfaces de validación, bien sea una api o algún mecanismo existente, por otra parte se propone una base de datos “nosql” que almacene cada request, para posteriormente, poder crear modelos predictivos de esta información, ejemplo: Ultimas 100 palabras mas usadas, o hasta brindar la posibilidad de acuerdo a la hora transmisión o armado de la oración, predisponer palabras que ayuden a la escritura del mensaje. 
Ejemplo: Inicio del mensaje (Hola [palabra propuesta a continuación = como] [propuesta = estas] [propuesta = ?], Todas estas propuestas estarán basadas en un modelo de comportamiento y cada una de esta ya contaran con traducciones en (morse, bits y lenguaje humano)

**Entre otras: **

- Definición de clases
- Validador de parámetro de velocidad
- Construcción de Frontend 
- Plantear la posibilidad de una integración continua 
- Plantear la posibilidad de un despliegue continuo
- Mejorar la collection de Postman 
- Documentación del producto
- Evaluar costos en AWS
- Posibilidad de autoescalado aws 
- Definición de Clusterización 
- Implementación de aplicaciones de monitoreo
	- NewRelic / Datadog
	- Graylog
