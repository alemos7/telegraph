package com.formacionbdi.springboot.app.telegraph.service.bits;

public class BitsRequest {

	private String velocity_operator = null;
	private BitsRule rule;
	private BitsTranslate translate;


	public BitsRequest() {
	}
	
	public BitsRequest(String velocity_operator, BitsRule rule, BitsTranslate translate) {
		this.velocity_operator = velocity_operator;
		this.rule = rule;
		this.translate = translate;
	}

	public BitsTranslate getTranslate() {
		return translate;
	}

	public void setTranslate(BitsTranslate translate) {
		this.translate = translate;
	}
	
	public BitsRule getRule() {
		return rule;
	}


	public void setRule(BitsRule rule) {
		this.rule = rule;
	}


	public String getVelocity_operator() {
		return velocity_operator;
	}

	public void setVelocity_operator(String velocity_operator) {
		this.velocity_operator = velocity_operator;
	}

	
}
