package com.formacionbdi.springboot.app.telegraph.service.morse;

public class MorseRule {
	private String short_value;
	private String long_value;
	private String space;
	private String sparator;
	private String end_first;
	private String end_second;
	
	public MorseRule() {
	}
	
	public MorseRule(String short_value, String long_value, String space, String sparator, String end_first,
			String end_second) {
		super();
		this.short_value = short_value;
		this.long_value = long_value;
		this.space = space;
		this.sparator = sparator;
		this.end_first = end_first;
		this.end_second = end_second;
	}
	
	public String getShort_value() {
		return short_value;
	}
	public void setShort_value(String short_value) {
		this.short_value = short_value;
	}
	public String getLong_value() {
		return long_value;
	}
	public void setLong_value(String long_value) {
		this.long_value = long_value;
	}
	public String getSpace() {
		return space;
	}
	public void setSpace(String space) {
		this.space = space;
	}
	public String getSparator() {
		return sparator;
	}
	public void setSparator(String sparator) {
		this.sparator = sparator;
	}
	public String getEnd_first() {
		return end_first;
	}
	public void setEnd_first(String end_first) {
		this.end_first = end_first;
	}
	public String getEnd_second() {
		return end_second;
	}
	public void setEnd_second(String end_second) {
		this.end_second = end_second;
	}
}
