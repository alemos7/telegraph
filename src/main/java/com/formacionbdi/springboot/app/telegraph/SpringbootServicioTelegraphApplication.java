package com.formacionbdi.springboot.app.telegraph;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootServicioTelegraphApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootServicioTelegraphApplication.class, args);
	}

}
