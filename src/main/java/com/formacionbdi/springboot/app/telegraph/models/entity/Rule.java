package com.formacionbdi.springboot.app.telegraph.models.entity;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Rule {
	
	@Id
	String id;
	String method;
	String velocity;
	String rule;
	
	public Rule(String method, String velocity, String rule) {
		this.method = method;
		this.velocity = velocity;
		this.rule = rule;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getVelocity() {
		return velocity;
	}

	public void setVelocity(String velocity) {
		this.velocity = velocity;
	}

	public String getRule() {
		return rule;
	}

	public void setRule(String rule) {
		this.rule = rule;
	}
	
	public String toString() {
		return "Method: "+method+" Velocity: "+velocity+" Rule: "+rule;
	}
	
}
