package com.formacionbdi.springboot.app.telegraph.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseController {

	final String ERROR = "No se pudo completar la traducción";
	
	public ResponseEntity<?> response(
			Integer code,
			String translation, 
			String human, 
			String bits,
			String morse) {
		
		if(code == 500) return error();
		
		switch (translation) {
	        case "bits2morse": 	if(bits =="") return error(); break;
	        case "morse2human": if(morse =="") return error(); break; 
	        case "human2morse": if(human == "") return error(); break; 
	        default: return error();
		}
		
		Map<String, String> json = new HashMap<>();
		if(human!="") json.put("human", human);
		if(bits	!="") json.put("bits", bits);
		if(morse!="") json.put("morse", morse);
		return new ResponseEntity<Map<String, String>>(json, (translation != "" ? HttpStatus.OK : HttpStatus.FORBIDDEN));
	}
	
	public ResponseEntity<?> error() {
		Map<String, String> json = new HashMap<>();
		json.put("Error", this.ERROR);
    	return new ResponseEntity<Map<String, String>>(json, HttpStatus.CONFLICT);
	}
}
