package com.formacionbdi.springboot.app.telegraph.service.morse;

public class MorseTranslate {
	private String text = null;
	private String morse = null;
	private String human = null;
	private String bits = null;
	

	public MorseTranslate() {
	}
	
	public MorseTranslate(String text, String morse, String human, String bits) {
		this.text 	= text;
		this.morse 	= morse;
		this.human 	= human;
		this.bits 	= bits;
	}

	public String getBits() {
		return bits;
	}

	public void setBits(String bits) {
		this.bits = bits;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String getMorse() {
		return morse;
	}
	
	public void setMorse(String morse) {
		this.morse = morse;
	}
	
	public String getHuman() {
		return human;
	}
	
	public void setHuman(String human) {
		this.human = human;
	} 
}
