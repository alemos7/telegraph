package com.formacionbdi.springboot.app.telegraph.service.rule;

public class RuleRequest {
	private String method;
	private String velocity;
	private String rule;
	
	public RuleRequest() {
	}
	public RuleRequest(String method, String velocity, String rule) {
		this.method = method;
		this.velocity = velocity;
		this.rule = rule;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getVelocity() {
		return velocity;
	}
	public void setVelocity(String velocity) {
		this.velocity = velocity;
	}
	public String getRule() {
		return rule;
	}
	public void setRule(String rule) {
		this.rule = rule;
	}
}
