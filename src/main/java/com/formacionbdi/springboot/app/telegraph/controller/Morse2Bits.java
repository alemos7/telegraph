package com.formacionbdi.springboot.app.telegraph.controller;

import org.json.JSONObject;

import com.formacionbdi.springboot.app.telegraph.service.RuleServiceImpl;

public class Morse2Bits {

	private String text;
	
	private String bit_init;
	private String bit_point;
	private String bit_stripe;
	private String bit_space;
	private String bit_sparator;
	private String bit_between;
	private String bit_end_first;
	private String bit_end_second;

	private String morse_sparator;
	private String morse_space;
	private String morse_short_value;
	private String morse_long_value;
	private String morse_end_first;
	private String morse_end_second;
	
	
	private String translation = "";

	public String translateMorse2Bits(String morse, RuleServiceImpl ruleService, String velocity) {
		this.getRules(ruleService, velocity, morse);
		
		this.translation += this.bit_init;
		String[] separateWord = this.text.split(this.morse_sparator);
		for(int i=0;i<separateWord.length ; i++)
		{
			String[] separateLetter = separateWord[i].split(this.morse_space);
			for(int ii=0;ii<separateLetter.length ; ii++)
			{
				for(int iii=0;iii<separateLetter[ii].length(); iii++)
				{
					if (String.valueOf(separateLetter[ii].charAt(iii)).equals(String.valueOf(this.morse_short_value))) {
						this.translation += this.bit_point;
					}
					if (String.valueOf(separateLetter[ii].charAt(iii)).equals(String.valueOf(this.morse_long_value))) {
						this.translation += this.bit_stripe;
					}
					if(separateLetter[ii].length() > 1 && separateLetter[ii].length()-1 != iii) this.translation += this.bit_space;
				}
				if(separateLetter.length > 1 && separateLetter.length-1 != ii) this.translation += this.bit_between;
			}
			if(separateWord.length > 1 && separateWord.length-1 != i) this.translation += this.bit_sparator;
		}
		return this.translation;
	}
	
	public void getRules(RuleServiceImpl ruleService, String velocity, String text) {
	
		this.text = text.toUpperCase();
		
		//Bits
		JSONObject ruleBits = new JSONObject(ruleService.getByMethodVelocity("bits2morse", velocity).getRule());
		this.bit_init 		= ruleBits.getString("init");
		this.bit_point 		= ruleBits.getString("point");
		this.bit_stripe 	= ruleBits.getString("stripe");
		this.bit_space 		= ruleBits.getString("space");
		this.bit_sparator 	= ruleBits.getString("sparator");
		this.bit_between	= ruleBits.getString("between");
		this.bit_end_first 	= ruleBits.getString("end_first");
		this.bit_end_second = ruleBits.getString("end_second");
		
		//Morse
		JSONObject ruleMorse 	= new JSONObject(ruleService.getByMethodVelocity("morse2human", velocity).getRule());
		this.morse_short_value 	= ruleMorse.getString("short_value");
		this.morse_long_value 	= ruleMorse.getString("long_value");
		this.morse_space 		= ruleMorse.getString("space");
		this.morse_sparator 	= ruleMorse.getString("sparator");
		this.morse_end_first 	= ruleMorse.getString("end_first");
		this.morse_end_second 	= ruleMorse.getString("end_second");
		
	}
	
}
