-- MySQL dump 10.13  Distrib 8.0.19, for Linux (x86_64)
--
-- Host: localhost    Database: meli
-- ------------------------------------------------------
-- Server version	8.0.19-0ubuntu0.19.10.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mapper`
--

DROP TABLE IF EXISTS `mapper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mapper` (
  `id` int NOT NULL AUTO_INCREMENT,
  `letter` char(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `morse` char(6) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mapper`
--

LOCK TABLES `mapper` WRITE;
/*!40000 ALTER TABLE `mapper` DISABLE KEYS */;
INSERT INTO `mapper` (`id`, `letter`, `morse`, `created_at`) VALUES (1,'A','.-','2020-04-23 22:42:49'),(2,'B','-...','2020-04-23 22:50:19'),(3,'C','-.-.','2020-04-23 22:50:19'),(4,'D','-..','2020-04-23 22:50:19'),(5,'E','.','2020-04-23 22:50:19'),(6,'F','..-.','2020-04-23 22:50:19'),(7,'G','--.','2020-04-23 22:50:19'),(8,'H','....','2020-04-23 22:50:19'),(9,'I','..','2020-04-23 22:50:19'),(10,'J','.---','2020-04-23 22:50:19'),(11,'K','-.-','2020-04-23 22:50:19'),(12,'L','.-..','2020-04-23 22:50:19'),(13,'M','--','2020-04-23 22:50:19'),(14,'N','-.','2020-04-23 22:50:19'),(15,'O','---','2020-04-23 22:50:19'),(16,'P','.--.','2020-04-23 22:50:19'),(17,'Q','--.-','2020-04-23 22:50:19'),(18,'R','.-.','2020-04-23 22:50:19'),(19,'S','...','2020-04-23 22:50:19'),(20,'T','-','2020-04-23 22:50:19'),(21,'U','..-','2020-04-23 22:50:19'),(22,'V','...-','2020-04-23 22:50:19'),(23,'W','.--','2020-04-23 22:50:19'),(24,'X','-..-','2020-04-23 22:50:19'),(25,'Y','-.--','2020-04-23 22:50:19'),(26,'Z','--..','2020-04-23 22:50:19'),(27,'0','-----','2020-04-23 22:50:19'),(28,'1','.----','2020-04-23 22:50:19'),(29,'2','..---','2020-04-23 22:50:19'),(30,'3','...--','2020-04-23 22:50:19'),(31,'4','....-','2020-04-23 22:50:19'),(32,'5','.....','2020-04-23 22:50:19'),(33,'6','-....','2020-04-23 22:50:19'),(34,'7','--...','2020-04-23 22:50:19'),(35,'8','---..','2020-04-23 22:50:19'),(36,'9','----.','2020-04-23 22:50:19'),(37,'null','.-.-.-','2020-04-23 22:50:19');
/*!40000 ALTER TABLE `mapper` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-30 19:39:27
