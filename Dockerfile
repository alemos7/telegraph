FROM openjdk:12
VOLUME /tmp
EXPOSE 8888
ADD ./target/springboot-servicio-telegraph-0.0.1-SNAPSHOT.jar servicio-telegraph.jar
ENTRYPOINT ["java","-jar","/servicio-telegraph.jar"]
