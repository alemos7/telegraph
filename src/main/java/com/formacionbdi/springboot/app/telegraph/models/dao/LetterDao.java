package com.formacionbdi.springboot.app.telegraph.models.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.formacionbdi.springboot.app.telegraph.models.entity.Letter;

public interface LetterDao extends PagingAndSortingRepository<Letter, Long> {

	public Letter findByLetter(String letter);
	public Letter findByMorse(String morse);
	
}
