package com.formacionbdi.springboot.app.telegraph.controller;

import org.json.JSONObject;

import com.formacionbdi.springboot.app.telegraph.service.RuleServiceImpl;
import com.formacionbdi.springboot.app.telegraph.service.bits.BitsRequest;

public class BitsController {

	private String init;
	private String point;
	private String stripe;
	private String space;
	private String sparator;
	private String between;
	private String end_first;
	private String end_second;
	
	
	public String decodeBits2Morse(BitsRequest request, RuleServiceImpl ruleService, String translation) {
		this.getRule(ruleService, request, translation);
		String morse = "";
		String[] prayerStart = request.getTranslate().getText().split(this.init);
		try {
			for(int i=0;i<prayerStart.length ; i++)
			{
				if(!prayerStart[i].isEmpty()) {
					String[] separateWord = prayerStart[i].split(this.sparator);
					for(int ii=0;ii<separateWord.length ; ii++)
					{
						if(!separateWord[ii].isEmpty()) {
							String[] separateLetter = separateWord[ii].split(this.between);
							for(int iii=0;iii<separateLetter.length ; iii++)
							{
								if(!separateLetter[iii].isEmpty()) {
									String[] morseTranslation = separateLetter[iii].split(this.space);
									for(int iiii=0;iiii<morseTranslation.length ; iiii++)
									{
										if(!morseTranslation[iiii].isEmpty()) {
										    if(morseTranslation[iiii].equals(this.point)) {
										    	morse += ".";
										    }
										    if(morseTranslation[iiii].equals(this.stripe)) {
										    	morse += "-";
										    }
										}
									}
									if(separateLetter.length-1 != iii) {
										morse += " ";
									}
								}
							}
						}
						if(separateWord.length > 1 && separateWord.length-1 != ii) {
							morse += "   ";
						}
					}
				}
			}
			return morse; 
		}catch (Exception e) {
			return null; 
		}
	}
	
	public void getRule(RuleServiceImpl ruleService, BitsRequest request, String translation) {
		
		if(validator(request)) {
			this.init 		= request.getRule().getInit();
			this.point 		= request.getRule().getPoint();
			this.stripe 	= request.getRule().getStripe();
			this.space 		= request.getRule().getSpace();
			this.sparator 	= request.getRule().getSparator();
			this.between	= request.getRule().getBetween();
			this.end_first 	= request.getRule().getEnd_first();
			this.end_second = request.getRule().getEnd_second();
		}else {

			JSONObject rule = new JSONObject(ruleService.getByMethodVelocity(translation, request.getVelocity_operator()).getRule());
			this.init 		= rule.getString("init");
			this.point 		= rule.getString("point");
			this.stripe 	= rule.getString("stripe");
			this.space 		= rule.getString("space");
			this.sparator 	= rule.getString("sparator");
			this.between	= rule.getString("between");
			this.end_first 	= rule.getString("end_first");
			this.end_second = rule.getString("end_second");
		}
		
	}
	
	@SuppressWarnings("unlikely-arg-type")
	public Boolean validator(BitsRequest request) {
		if(request.getRule() == null) {
			return false;
		}
		if (
			request.getRule().getInit() != "" &&
			request.getRule().getPoint() != "" &&
			request.getRule().getStripe() != "" &&
			request.getRule().getSpace() != "" &&
			request.getRule().getSparator() != "" &&
			request.getRule().getBetween() != ""
			) {
			return true;
		}
		return false;
	}
	
}
