package com.formacionbdi.springboot.app.telegraph.controller;

import org.json.JSONObject;

import com.formacionbdi.springboot.app.telegraph.service.ILatterService;
import com.formacionbdi.springboot.app.telegraph.service.RuleServiceImpl;
import com.formacionbdi.springboot.app.telegraph.service.human.HumanRequest;

public class HumanController {
	
	private String sparator;
	private String end_first;
	private String end_second;
	private String text;
	
	private String morse_sparator;
	private String morse_space;
	
	private String translation = "";

	public String human2morse(HumanRequest request, RuleServiceImpl ruleService, ILatterService letterService, String translation, String text) {
		
		this.getRule(ruleService, request, translation, text);
		
		String[] separateWord = this.text.split(this.sparator);
		for(int i=0;i<separateWord.length ; i++)
		{
			for(int ii=0;ii<separateWord[i].length(); ii++)
			{
				try {
					this.translation += letterService.findByLetter(String.valueOf(separateWord[i].charAt(ii))).getMorse(); 
					
					if(separateWord[i].length() > 1 && separateWord[i].length()-1 != ii) {
						this.translation += this.morse_space;
					}
				}catch (Exception e) {
					return this.translation = null;
				}
			}
			if(separateWord.length > 1 && separateWord.length-1 != i) {
				this.translation += this.morse_sparator;
			}
		}
		return this.translation;
	}
	
	public String getTranslation() {
		return translation;
	}

	public void setTranslation(String translation) {
		this.translation = translation;
	}
	
	public void getRule(RuleServiceImpl ruleService, HumanRequest request, String translation, String text) {
		if(validator(request)) {
			this.sparator	= request.getRule().getSparator();
			this.end_first	= request.getRule().getEnd_first();
			this.end_second	= request.getRule().getEnd_second();
			this.text 		= request.getTranslate().getText().toUpperCase();
		}else {
			JSONObject rule = new JSONObject(ruleService.getByMethodVelocity(translation, request.getVelocity_operator()).getRule());
			this.sparator	= rule.getString("sparator");
			this.end_first	= rule.getString("end_first");
			this.end_second	= rule.getString("end_second");
			this.text		= text.toUpperCase();
		}
		this.getRuleMorse(ruleService, request.getVelocity_operator());
	}
	
	public void getRuleMorse(RuleServiceImpl ruleService, String velocity) {
		JSONObject ruleMorse = new JSONObject(ruleService.getByMethodVelocity("morse2human", velocity).getRule());
		this.morse_sparator = ruleMorse.getString("sparator");
		this.morse_space 	= ruleMorse.getString("space");
	}
	
	@SuppressWarnings("unlikely-arg-type")
	public Boolean validator(HumanRequest request) {
		if(request.getRule() == null) {
			return false;
		}
		if (
			request.getRule().getSparator() != "" &&
			request.getRule().getEnd_first() != "" &&
			request.getRule().getEnd_second() != ""
			) {
			return true;
		}
		return false;
	}
	
}
