package com.formacionbdi.springboot.app.telegraph.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formacionbdi.springboot.app.telegraph.models.entity.Rule;

@Service
public class RuleServiceImpl {
	@Autowired
	private IRuleService iruleService;
	
	public Rule create(String method, String velocity, String rule) {
		return iruleService.save(new Rule(method, velocity, rule));
	}
	
	public List<Rule> getAll() {
		return iruleService.findAll();
	}

	public Rule getByMethod(String method) {
		return iruleService.findByMethod(method);
	}
	
	public Rule getByMethodVelocity(String method, String velocity) {
		return iruleService.findByMethodAndVelocity(method, velocity);
	}
	
	public Rule update(String method, String velocity, String rule) {
		
		Rule ruleDefinition = iruleService.findByMethod(method);
		ruleDefinition.setMethod(method);
		ruleDefinition.setVelocity(velocity);
		ruleDefinition.setRule(rule);
		return iruleService.save(ruleDefinition);
	}
}
